# What is this?

This is:

* companion program used to test [Dummy SMTP Server](https://bitbucket.org/rdyn/dummy-smtp-server)
* place where you can come and copy Java code used to send emails in different forms and shapes

# What kind od messages it sends

* simplest of all: text email - `sendSimpleMessage()`
* just as the one above, but makes it a MIME message - `sendSimpleMimeMessage()`
* email with attachment - `sendImageAttachment()`
* email with inline images (and HTML) - `sendImageInline()`
* HTML email with content created from a template parsed by Apache Velocity - `sendHtmlFromTemplate()`


# How to use it

* import project to Spring Tool Suite
* change host/port settings in src/main/resources/application.properties, i.e.

	spring.mail.host = localhost
	spring.mail.port = 2025

* run TestmailerApplication class
* I said it was a dummy thing, haven't I?

