package com.ravendyne.web.testmailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestmailerApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(TestmailerApplication.class, args);
	}
}
