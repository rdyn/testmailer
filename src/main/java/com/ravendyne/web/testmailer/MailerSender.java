package com.ravendyne.web.testmailer;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

@Component
public class MailerSender implements CommandLineRunner
{
    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    VelocityEngine velocityEngine;

    @Override
    public void run(String... arg0) throws Exception
    {
        sendSimpleMessage();
        sendSimpleMimeMessage();
        sendImageAttachment();
        sendImageInline();
        sendHtmlFromTemplate();
    }

    private void sendHtmlFromTemplate()
    {
        MimeMessage message = javaMailSender.createMimeMessage();

        try
        {
            // use the true flag to indicate you need a multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("sender@example.com");
            helper.setTo("test@host.com");
            helper.setSubject("check out this html template");

            // Here's how you use Velocity, in a nutshell
            Map<String, Object> model = new HashMap<>();
            User user = new User();
            user.setUserName("User's username");
            user.setEmailAddress("users_email@address.com");
            model.put("user", user);
            String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "template.vm", "UTF-8", model);
            
            // Now just set the result as message text,
            // and make sure the second parameter is 'true'
            // which means the text is HTML
            helper.setText(text, true);
        }
        catch (MessagingException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try
        {
            javaMailSender.send(message);
        }
        catch (MailException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendImageInline()
    {
        MimeMessage message = javaMailSender.createMimeMessage();

        try
        {
            // use the true flag to indicate you need a multipart message
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("sender@example.com");
            helper.setTo("test@host.com");
            helper.setSubject("check out this inline image");

            // use the true flag to indicate the text included is HTML
            helper.setText("<html><body><img src=''cid:identifier1234''></body></html>", true);

            ClassPathResource res = new ClassPathResource("image-01.png");
            helper.addInline("identifier1234", res);
        }
        catch (MessagingException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try
        {
            javaMailSender.send(message);
        }
        catch (MailException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendImageAttachment()
    {
        MimeMessage message = javaMailSender.createMimeMessage();

        // use the true flag to indicate you need a multipart message
        MimeMessageHelper helper;
        try
        {
            helper = new MimeMessageHelper(message, true);

            helper.setFrom("sender@example.com");
            helper.setTo("test@host.com");
            helper.setSubject("check out this attachment");
            helper.setText("Check out this image!");

            ClassPathResource file = new ClassPathResource("image-01.png");
            helper.addAttachment("CoolImage.jpg", file);
        }
        catch (MessagingException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try
        {
            javaMailSender.send(message);
        }
        catch (MailException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSimpleMimeMessage()
    {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try
        {
            helper.setFrom("sender@example.com");
            helper.setTo("test@host.com");
            helper.setSubject("mime email subject");
            helper.setText("Thank you for ordering!");
        }
        catch (MessagingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try
        {
            javaMailSender.send(message);
        }
        catch (MailException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void sendSimpleMessage()
    {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("receiver@example.com");
        msg.setFrom("sender@example.com");
        msg.setText("Test email");
        msg.setSubject("email subject");

        try
        {
            javaMailSender.send(msg);
        }
        catch (MailException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
