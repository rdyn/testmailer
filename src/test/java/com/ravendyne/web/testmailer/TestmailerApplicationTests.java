package com.ravendyne.web.testmailer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestmailerApplication.class)
public class TestmailerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
